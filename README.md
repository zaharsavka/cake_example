# test.cake.com

To run this application you need to install cakephp instalation and use DB dump from file db_dump.sql

In this application you can:
- register and login as a user
- make an appointments with registered doctors
- sort an active appointments
- see all appointments and their statuses
- register and login as a doctor
- see, approve, decline and delete appointments
- sort an active appointments
- Register/Login with Google
- Google Calendar Events
- Simple adaptive inline Front-end for convenience

Google App Configured to work with domain "test.cake.com"





