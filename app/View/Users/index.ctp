<div style="width: 80%; max-width: 1200px; margin:50px auto; padding: 20px; background: #fff">
    <span style="float: right"><?php echo $this->Html->link("Logout", array('action' => 'logout')); ?></span>
    <p>This is your Dashboard. You can make an appointment here:</p>
    <p><a href="/users/appointment">Make an appointment</a></p>
    <?php if(isset($appointments) && !empty($appointments)){ ?>
        <h3>Your active appointments</h3>
        <table>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('fullname', 'Doctor name'); ?></th>
                <th><?php echo $this->Paginator->sort('date', 'Date'); ?></th>
                <th><?php echo $this->Paginator->sort('start_time', 'Start time'); ?></th>
                <th><?php echo $this->Paginator->sort('end_time', 'End time'); ?></th>
                <th><?php echo $this->Paginator->sort('created', 'Created'); ?></th>
                <th><?php echo $this->Paginator->sort('status', 'Status'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php $count = 0; ?>
            <?php foreach($appointments as $appointment): ?>
                <?php $count ++;?>
                <?php if($count % 2): echo '<tr>'; else: echo '<tr class="zebra">' ?>
                <?php endif; ?>
                    <td style="text-align: center;"><?php echo h($appointment['Doctor']['fullname']); ?></td>
                    <td style="text-align: center;"><?php echo h($appointment['Appointment']['date']); ?></td>
                    <td style="text-align: center;"><?php echo h($appointment['Appointment']['start_time']); ?></td>
                    <td style="text-align: center;"><?php echo h($appointment['Appointment']['end_time']); ?></td>
                    <td style="text-align: center;"><?php echo h($this->Time->niceShort($appointment['Appointment']['created'])); ?></td>
                    <td style="text-align: center;"><?php if($appointment['Appointment']['status'] == 0){
                        echo "Pending";
                    } elseif($appointment['Appointment']['status'] == 1) {
                        echo "Approved";
                    } else {
                        echo "Declined";
                    }?></td>
                </tr>
                <?php endforeach; ?>
            <?php unset($appointment); ?>
            </tbody>
        </table>
        <div style="text-align: center; width: 100%;">
            <?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('class' => 'numbers')); ?>
            <?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
        </div>
    <?php } else { ?>
        <h3>Your have no appointments yet</h3>
    <?php } ?>
</div>

