<div style="width: 80%; max-width: 800px; margin:50px auto; padding: 50px; background: #fff; text-align: center">
    <p>Welcome to Cake Test Assignment project!</p>
    <p>Here you can make an appointment with doctors, or get appointments from the patients</p>
    <h3>Sign in to proceed:</h3>
    <a class="btn btn-default google" href="/users/googlelogin"> <i class="fa fa-google-plus modal-icons"></i> I'm a patient</a> or <a class="btn btn-default google" href="/doctors/googlelogin"> <i class="fa fa-google-plus modal-icons"></i> I'm a doctor</a>
</div>