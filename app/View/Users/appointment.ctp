<div style="width: 80%; max-width: 1200px; margin:50px auto; padding: 20px; background: #fff">
    <span style="float: right"><?php echo $this->Html->link("Logout", array('action' => 'logout')); ?></span>
    <a href="/users/">Go to profile</a>

    <h1>Make an appointment</h1>
    <?php if (isset($doctors) && !empty($doctors)) { ?>
        <table>
            <tr>
                <th><?php echo $this->Paginator->sort('username', 'Doctors Name'); ?>  </th>
                <th><?php echo $this->Paginator->sort('type', 'Doctors Type'); ?>  </th>
                <th>Date</th>
                <th>Time</th>
                <th>End Time</th>
                <th>Submit</th>
            </tr>
            <?php $count = 0; ?>
            <?php foreach ($doctors as $doctor): ?>
                <tr>
                    <?php echo $this->Form->create('send-' . $count, array('default' => false)); ?>
                    <?php echo $this->Form->input('send.doctor_id', array('type' => 'hidden', 'value' => h($doctor['Doctor']['id']))); ?>
                    <?php echo $this->Form->input('send.user_id', array('type' => 'hidden', 'value' => $userId)); ?>
                    <td><?php echo h($doctor['Doctor']['fullname']); ?></td>
                    <td><?php echo h($doctor['Doctor']['type']); ?></td>
                    <td><?php echo $this->Form->date('send.date', array('selected' => date("d-m-Y"))); ?></td>
                    <td><?php echo $this->Form->time('send.start_time'); ?></td>
                    <td><?php echo $this->Form->time('send.end_time'); ?></td>
                    <td>
                        <div id="appStatus-<?php echo $count; ?>"><?php echo $this->Form->end('Submit'); ?></div>
                    </td>
                </tr>
                <?php
                $data = $this->Js->get('#send-' . $count . 'AppointmentForm')->serializeForm(array('isForm' => true, 'inline' => true));
                $this->Js->get('#send-' . $count . 'AppointmentForm')->event(
                    'submit',
                    $this->Js->request(
                        array('action' => 'app', 'controller' => 'users'),
                        array(
                            'update' => 'body',
                            'data' => $data,
                            'async' => true,
                            'dataExpression' => true,
                            'method' => 'POST'
                        )
                    )
                );
                echo $this->Js->writeBuffer();
                ?>
                <?php $count++; ?>
            <?php endforeach; ?>
        </table>
        <div style="text-align: center; width: 100%;">
            <?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('class' => 'numbers')); ?>
            <?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
        </div>
    <?php } else { ?>
        <h3>There is no doctors yet, sorry for that</h3>
    <?php } ?>
</div>
