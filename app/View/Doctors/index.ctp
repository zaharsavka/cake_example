<div style="width: 80%; max-width: 1200px; margin:50px auto; padding: 20px; background: #fff">
    <span style="float: right"><?php echo $this->Html->link("Logout", array('action' => 'logout')); ?></span>
    <h1>Doctor Dashboard</h1>
    <?php if(isset($appointments)&& !empty($appointments)){?>
        <h2>Your active appointments</h2>
        <table>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('email', 'Email'); ?></th>
                <th><?php echo $this->Paginator->sort('date', 'Date'); ?></th>
                <th><?php echo $this->Paginator->sort('start_time', 'Start Time'); ?></th>
                <th><?php echo $this->Paginator->sort('end_time', 'End Time'); ?></th>
                <th><?php echo $this->Paginator->sort('created', 'Created'); ?></th>
                <th><?php echo $this->Paginator->sort('status', 'Status'); ?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $count = 0; ?>
            <?php foreach($appointments as $appointment): ?>
                <?php $count ++;?>
                <?php if($count % 2): echo '<tr>'; else: echo '<tr class="zebra">' ?>
                <?php endif; ?>
                    <td style="text-align: center;"><?php echo h($appointment['User']['email']); ?></td>
                    <td style="text-align: center;"><?php echo h($appointment['Appointment']['date']); ?></td>
                    <td style="text-align: center;"><?php echo h($appointment['Appointment']['start_time']); ?></td>
                    <td style="text-align: center;"><?php echo h($appointment['Appointment']['end_time']); ?></td>
                    <td style="text-align: center;"><?php echo $this->Time->niceShort(h($appointment['Appointment']['created'])); ?></td>
                    <td style="text-align: center;">
                    <?php if($appointment['Appointment']['status'] == 0){
                        echo "Pending";
                    } elseif($appointment['Appointment']['status'] == 1) {
                        echo "Approved";
                    } else {
                        echo "Declined";
                    }?></td>
                    <td style="text-align: center;">
                    <?php
                        if( $appointment['Appointment']['status'] == 0){
                            echo $this->Html->link(    "Approve", array('action'=>'approve', h($appointment['Appointment']['id'])));?> | <?php
                            echo $this->Html->link(    "Decline", array('action'=>'decline', h($appointment['Appointment']['id'])));
                        } elseif($appointment['Appointment']['status'] == 1){
                            echo $this->Html->link(    "Decline", array('action'=>'decline', h($appointment['Appointment']['id'])));
                        } elseif($appointment['Appointment']['status'] == 2) {
                            echo $this->Html->link(    "Approve", array('action'=>'approve', h($appointment['Appointment']['id'])));
                        };?> | <?php
                    echo $this->Html->link(    "Delete",   array('action'=>'delete', h($appointment['Appointment']['id'])) ); ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            <?php unset($appointment); ?>
            </tbody>
        </table>
        <div style="text-align: center; width: 100%;">
            <?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('class' => 'numbers')); ?>
            <?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
        </div>
        <script>
            $('.update').click(function(){
                $.ajax({
                    url: "/doctors/update",
                    type: 'POST',
                    data: {"id": this.title, "status": this.name },
                    success: function(data){
                        $( 'span.change' ).replaceWith( "Updated" );
                    }
                });
            });
        </script>
    <?php } else {?>
        <h3>There is no appointments yet. Come back later</h3>
    <?php } ?>
</div>

