<?php
ini_set('date.timezone', 'Europe/Kiev');

class DoctorsController extends AppController
{

    var $components = array('Security');

    public $uses = array(
        'Doctor',
        'User'
    );

    public $paginate = array(
        'limit' => 25,
        'conditions' => array('status' => '1'),
        'order' => array('Doctor.username' => 'asc')
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('login', 'add', 'google_login', 'googlelogin');
        $this->Security->blackHoleCallback = 'fail';
    }

    function fail()
    {
        throw new NotFoundException('Not allowed action');
    }

    public function login()
    {

        if ($this->Session->check('Auth.User') && $this->Auth->user('fullname')) {
            $this->redirect(array('action' => 'index'));
        }

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Session->setFlash(__('Welcome, ' . $this->Auth->user('username'), 'flash_success'));
                $this->redirect(array('controller' => 'doctors', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('Invalid username or password'));
            }
        }
    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function index()
    {

        if ($this->Session->check('Auth.User')) {
            if ($this->Auth->user('fullname')) {
                $userId = $this->Auth->user('id');
                $this->loadModel('Appointment');
                $this->paginate = array(
                    'limit' => 3,
                    'conditions' => array(
                        'Appointment.doctor_id' => $userId,
                        'Appointment.status' => array(0, 1, 2)
                    ),
                );
                $appointments = $this->paginate('Appointment');
                $this->set(compact('appointments'));
            } else {
                $this->redirect(array('controller' => 'doctors', 'action' => 'login'));
            }
        } else {
            $this->redirect(array('controller' => 'doctors', 'action' => 'login'));
        }
    }

    public function approve($id = null)
    {

        if (!$id) {
            $this->Session->setFlash('Please provide an appointment id');
            $this->redirect(array('action' => 'index'));
        }

        $this->loadModel('Appointment');
        $this->Appointment->id = $id;
        if (!$this->Appointment->exists()) {
            $this->Session->setFlash('Invalid id provided');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Appointment->saveField('status', 1)) {


            $appointment_data = $this->Appointment->find('first', array(
                'conditions' => array('Appointment.id' => $id)
            ));

            $credentials = Configure::read('Credentials');

            $client_id = $credentials['client_id'];
            $client_secret = $credentials['client_secret'];
            $redirect_url = $credentials['redirect_url'];
            $developer_key = $credentials['developer_key'];

            require_once '../Vendor/Google/src/Google/autoload.php';
            $client = new Google_Client();
            $client->setClientId($client_id);
            $client->setClientSecret($client_secret);
            $client->setRedirectUri($redirect_url);
            $client->setDeveloperKey($developer_key);
            $client->setScopes(array('https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me', 'https://www.googleapis.com/auth/calendar'));
            $client->setApprovalPrompt('auto');
            $client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($client);
            $service->events->quickAdd(
                'primary',
                "Appointment with " . $appointment_data['User']['email'] . " at Caketest on " . $appointment_data['Appointment']['date'] . " " . substr($appointment_data['Appointment']['start_time'], 0, 5) . " - " . substr($appointment_data['Appointment']['end_time'], 0, 5));

            $this->Session->setFlash("Appointment approved!", 'flash_success');

            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Appointment was not updated'));
        $this->redirect(array('action' => 'index'));
    }

    public function decline($id = null)
    {

        if (!$id) {
            $this->Session->setFlash('Please provide an appointment id');
            $this->redirect(array('action' => 'index'));
        }

        $this->loadModel('Appointment');

        $this->Appointment->id = $id;
        if (!$this->Appointment->exists()) {
            $this->Session->setFlash('Invalid id provided');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Appointment->saveField('status', 2)) {
            $this->Session->setFlash('Appointment declined', 'flash_success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Appointment was not updated'));
        $this->redirect(array('action' => 'index'));
    }

    public function delete($id = null)
    {

        if (!$id) {
            $this->Session->setFlash('Please provide an appointment id');
            $this->redirect(array('action' => 'index'));
        }

        $this->loadModel('Appointment');

        $this->Appointment->id = $id;
        if (!$this->Appointment->exists()) {
            $this->Session->setFlash('Invalid id provided');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Appointment->saveField('status', 3)) {
            $this->Session->setFlash('Appointment deleted', 'flash_success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Appointment was not updated'));
        $this->redirect(array('action' => 'index'));
    }

    public function update()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Appointment');
            $data = array(
                'id' => $this->request->data['id'],
                'status' => $this->request->data['status']
            );
            if ($this->Appointment->save($data)) {
                echo "Done!";
                die;
            } else {
                echo "DB error!";
                die;
            }
        }
    }


    /**
     * This function will makes Oauth Api reqest
     */
    public function googlelogin()
    {
        $this->autoRender = false;
        $credentials = Configure::read('Credentials');

        $client_id = $credentials['client_id'];
        $client_secret = $credentials['client_secret'];
        $redirect_url = $credentials['redirect_url'];
        $developer_key = $credentials['developer_key'];
        require_once '../Vendor/Google/src/Google/autoload.php';
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_url);
        $client->setDeveloperKey($developer_key);
        $client->setScopes(array('https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me', 'https://www.googleapis.com/auth/calendar'));
        $client->setApprovalPrompt('auto');
        $url = $client->createAuthUrl();
        $this->redirect($url);
    }

    /**
     * This function will handle Oauth Api response
     */
    public function google_login()
    {
        $this->autoRender = false;
        $credentials = Configure::read('Credentials');

        $client_id = $credentials['client_id'];
        $client_secret = $credentials['client_secret'];
        $redirect_url = $credentials['redirect_url'];
        $developer_key = $credentials['developer_key'];
        require_once '../Vendor/Google/src/Google/autoload.php';
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_url);
        $client->setDeveloperKey($developer_key);
        $client->setScopes(array('https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me', 'https://www.googleapis.com/auth/calendar'));
        $client->setApprovalPrompt('auto');

        $oauth2 = new Google_Service_Oauth2($client);
        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']); // Authenticate
            $_SESSION['access_token'] = $client->getAccessToken(); // get the access token here
        }

        if (isset($_SESSION['access_token'])) {
            $client->setAccessToken($_SESSION['access_token']);
        }

        if ($client->getAccessToken()) {
            $_SESSION['access_token'] = $client->getAccessToken();
            $user = $oauth2->userinfo->get();
            try {
                if (!empty($user)) {
                    $result = $this->Doctor->findByEmail($user['email']);
                    if (!empty($result)) {
                        if ($this->Auth->login($result['Doctor'])) {
                            $this->Session->setFlash('GOOGLE_LOGIN_SUCCESS', 'default', array('class' => 'message success'), 'flash_success');
                            $this->redirect(array('controller' => 'doctors', 'action' => 'index'));
                        } else {
                            $this->Session->setFlash('GOOGLE_LOGIN_FAILURE', 'default', array('class' => 'message error'), 'error');
                            $this->redirect(array('controller' => 'users', 'action' => 'login'));
                        }

                    } else {
                        $data = array();
                        $data['username'] = $user['email'];
                        $data['type'] = 'doctor';
                        $data['fullname'] = $user['name'];
                        $data['password'] = date('Y-m-d h:i:s');
                        $data['email'] = $user['email'];
                        $data['first_name'] = $user['given_name'];
                        $data['last_name'] = $user['family_name'];
                        $data['social_id'] = $user['id'];
                        $data['picture'] = $user['picture'];
                        $data['gender'] = $user['gender'] == 'male' ? 'm' : 'f';
                        $data['user_level_id'] = 1;
                        $data['uuid'] = String::uuid();
                        if ($this->Doctor->save($data)) {
                            $data['id'] = $this->Doctor->getLastInsertID();
                            if ($this->Auth->login($data)) {
                                $this->Session->setFlash('GOOGLE_LOGIN_SUCCESS', 'default', array('class' => 'message success'), 'flash_success');
                                $this->redirect(array('controller' => 'doctors', 'action' => 'index'));
                            } else {
                                $this->Session->setFlash('GOOGLE_LOGIN_FAILURE', 'default', array('class' => 'message error'), 'error');
                                $this->redirect(array('controller' => 'users', 'action' => 'login'));
                            }
                        }
                    }

                } else {
                    $this->Session->setFlash('GOOGLE_LOGIN_FAILURE', 'default', array('class' => 'message error'), 'error');
                    $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
            } catch (Exception $e) {
                $this->Session->setFlash('GOOGLE_LOGIN_FAILURE', 'default', array('class' => 'message error'), 'error');
                $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
        }

        exit;
    }

}