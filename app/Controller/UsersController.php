<?php
ini_set('date.timezone', 'Europe/Kiev');

class UsersController extends AppController
{
    var $components = array('Security');

    public $uses = array(
        'Doctor',
        'User'
    );

    public $helpers = array('Paginator');

    public $paginate = array(
        'limit' => 25,
        'conditions' => array('status' => '1'),
        'order' => array('User.username' => 'asc')
    );


    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('login', 'add', 'google_login', 'googlelogin');
        $this->Security->unlockedActions = array('app');
        $this->Security->blackHoleCallback = 'fail';
    }

    public function login()
    {

        if ($this->Session->check('Auth.User') && !$this->Auth->user('fullname')) {
            $this->redirect(array('action' => 'index'));
        }

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Session->setFlash(__('Welcome, ' . $this->Auth->user('username'), 'flash_success'));
                $this->redirect(array('controller' => 'users', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('Invalid username or password'));
            }
        }
    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function index()
    {
        if ($this->Session->check('Auth.User') && !$this->Auth->user('fullname')) {
            $userId = $this->Auth->user('id');
            $this->loadModel('Appointment');
            $this->paginate = array(
                'limit' => 3,
                'conditions' => array('Appointment.user_id' => $userId),
            );
            $appointments = $this->paginate('Appointment');
            $this->set(compact('appointments'));
        } else {
            $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
    }

    public function appointment()
    {
        if ($this->Session->check('Auth.User') && !$this->Auth->user('fullname')) {
            $this->paginate = array(
                'Doctor' => array(
                    'limit' => 2,
                    'table' => 'doctors'
                ),
            );

            $doctors = $this->paginate('Doctor');
            $userId = $this->Auth->user('id');
            $this->set('doctors', $doctors);
            $this->set('userId', $userId);
        } else {
            $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
    }

    public function app()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Appointment');
            $data = $this->request->data['send'];

            $this->Appointment->validator()
                ->add('doctor_id', 'required', array(
                    'rule' => array('notEmpty'),
                    'message' => 'Doctor ID is required'
                ))
                ->add('user_id', 'required', array(
                    'rule' => array('notEmpty'),
                    'message' => 'User ID is required'
                ))
                ->add('date', 'required', array(
                    'rule' => array('date'),
                    'message' => 'Enter valid date',
                    'allowEmpty' => false
                ))
                ->add('start_time', 'required', array(
                    'rule' => array('time'),
                    'message' => 'Enter valid start time',
                    'allowEmpty' => false
                ))
                ->add('end_time', 'required', array(
                    'rule' => array('time'),
                    'message' => 'Enter valid end time',
                    'allowEmpty' => false
                ));


            if ($this->Appointment->validates()) {
                if ($data['start_time'] < $data['end_time']) {
                    $conflicts = $this->Appointment->find('all', array(
                        'conditions' => array(
                            'Appointment.date' => $data['date'],
                            'Appointment.doctor_id' => $data['doctor_id'],
                        )
                    ));
                    foreach ($conflicts as $conflict) {
                        $end_time = substr($conflict['Appointment']['end_time'], 0, 5);
                        $start_time = substr($conflict['Appointment']['start_time'], 0, 5);
                        if ($data['end_time'] > $start_time && $data['start_time'] < $end_time) {
                            $this->Session->setFlash('This time is already busy');
                            return $this->redirect(array('action' => 'appointment'));
                        }
                    }
                    if ($this->Appointment->save($data)) {

                        $credentials = Configure::read('Credentials');

                        $client_id = $credentials['client_id'];
                        $client_secret = $credentials['client_secret'];
                        $redirect_url = $credentials['redirect_url'];
                        $developer_key = $credentials['developer_key'];
                        require_once '../Vendor/Google/src/Google/autoload.php';
                        $client = new Google_Client();
                        $client->setClientId($client_id);
                        $client->setClientSecret($client_secret);
                        $client->setRedirectUri($redirect_url);
                        $client->setDeveloperKey($developer_key);
                        $client->setScopes(array('https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me', 'https://www.googleapis.com/auth/calendar'));
                        $client->setApprovalPrompt('auto');
                        $client->setAccessToken($_SESSION['access_token']);
                        $service = new Google_Service_Calendar($client);
                        $service->events->quickAdd(
                            'primary',
                            "Appointment at Caketest on " . $data['date'] . " " . substr($data['start_time'], 0, 5) . " - " . substr($data['end_time'], 0, 5));

                        $this->Session->setFlash('Appointment saved!', 'flash_success');
                        return $this->redirect(array('action' => 'appointment'));
                    } else {
                        $this->Session->setFlash('Saving error!');
                        return $this->redirect(array('action' => 'appointment'));
                    }
                } else {
                    $this->Session->setFlash('Wrong time period!');
                    return $this->redirect(array('action' => 'appointment'));
                }
            } else {
                $this->Session->setFlash($errors = $this->Appointment->validationErrors);
            }

        }
    }


    /**
     * This function will makes Oauth Api reqest
     */
    public function googlelogin()
    {
        $this->autoRender = false;
        $client_id = '846783401367-f789nh8sd37a5rrf1v084lb2kqt2st0p.apps.googleusercontent.com';
        $client_secret = 'mJ5aG0GKa0aR0N1Tsv-_pXBi';
        $redirect_url = 'http://test.cake.com/users/google_login';
        $developer_key = 'AIzaSyBACqj72-2kiXfe2AikmA2LP2Ii9kfNsiQ';
        require_once '../Vendor/Google/src/Google/autoload.php';
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_url);
        $client->setDeveloperKey($developer_key);
        $client->setScopes(array('https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/calendar'));
        $client->setApprovalPrompt('auto');
        $url = $client->createAuthUrl();
        $this->redirect($url);
    }

    /**
     * This function will handle Oauth Api response
     */
    public function google_login()
    {
        $this->autoRender = false;
        $client_id = '846783401367-f789nh8sd37a5rrf1v084lb2kqt2st0p.apps.googleusercontent.com';
        $client_secret = 'mJ5aG0GKa0aR0N1Tsv-_pXBi';
        $redirect_url = 'http://test.cake.com/users/google_login';
        $developer_key = 'AIzaSyBACqj72-2kiXfe2AikmA2LP2Ii9kfNsiQ';
        require_once '../Vendor/Google/src/Google/autoload.php';
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_url);
        $client->setDeveloperKey($developer_key);
        $client->setScopes(array('https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/calendar'));
        $client->setApprovalPrompt('auto');

        $oauth2 = new Google_Service_Oauth2($client);
        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']); // Authenticate
            $_SESSION['access_token'] = $client->getAccessToken(); // get the access token here
        }

        if (isset($_SESSION['access_token'])) {
            $client->setAccessToken($_SESSION['access_token']);
        }

        if ($client->getAccessToken()) {
            $_SESSION['access_token'] = $client->getAccessToken();
            $user = $oauth2->userinfo->get();
            try {
                if (!empty($user)) {
                    $result = $this->User->findByEmail($user['email']);
                    if (!empty($result)) {
                        if ($this->Auth->login($result['User'])) {
                            $this->Session->setFlash('GOOGLE_LOGIN_SUCCESS', 'default', array('class' => 'message success'), 'flash_success');
                            $this->redirect(array('controller' => 'users', 'action' => 'index'));
                        } else {
                            $this->Session->setFlash('GOOGLE_LOGIN_FAILURE', 'default', array('class' => 'message error'), 'error');
                            $this->redirect(array('controller' => 'users', 'action' => 'login'));
                        }

                    } else {
                        $data = array();
                        $data['username'] = $user['email'];
                        $data['password'] = date('Y-m-d h:i:s');
                        $data['email'] = $user['email'];
                        $data['social_id'] = $user['id'];
                        $data['picture'] = $user['picture'];
                        $data['gender'] = $user['gender'] == 'male' ? 'm' : 'f';
                        $data['user_level_id'] = 1;
                        $data['uuid'] = String::uuid();
                        if (!empty($data)) {
                            if ($this->User->save($data)) {
                                $data['id'] = $this->User->getLastInsertID();
                                if ($this->Auth->login($data)) {
                                    $this->Session->setFlash('GOOGLE_LOGIN_SUCCESS', 'default', array('class' => 'message success'), 'flash_success');
                                    $this->redirect(array('controller' => 'users', 'action' => 'index'));
                                } else {
                                    $this->Session->setFlash('GOOGLE_LOGIN_FAILURE', 'default', array('class' => 'message error'), 'error');
                                    $this->redirect(array('controller' => 'users', 'action' => 'login'));
                                }

                            }
                        }
                    }

                } else {
                    $this->Session->setFlash('GOOGLE_LOGIN_FAILURE', 'default', array('class' => 'message error'), 'error');
                    $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
            } catch (Exception $e) {
                $this->Session->setFlash('GOOGLE_LOGIN_FAILURE', 'default', array('class' => 'message error'), 'error');
                $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
        }

        exit;
    }

}